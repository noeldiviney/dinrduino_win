#!C:\Program Files\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#param(
#         [string]$DRIVE      = "DRIVE", 
#		 [string]$FOLDER     = "FOLDER", 
#		 [string]$VERSION    = "VERSION",  
#		 [string]$VENDOR     = "VENDOR" );
#Read-Host -Prompt "Pausing:  Press any key to continue"
           
$INSTALL_PATH              = "C:\J-Tagit"
$ARDUINO_VERSION           = "1.8.18"
$BOARD_VENDOR              = "Eicon"
$BOARD_VENDOR_LC           = "eicon"
$MSYS64_PATH               = "${INSTALL_PATH}\msys64"
$DLOAD_PATH                = "${INSTALL_PATH}\dload"
$7ZIP_PATH                 = "C:\Program Files\7-Zip"

$MSYS2_LATEST              = "http://repo.msys2.org/distrib/msys2-x86_64-latest.tar.xz"
$ZIP_PATH                  = "${DLOAD_PATH}\msys2-x86_64-latest.tar.xz"
$TAR_PATH                  = "${DLOAD_PATH}\msys2-x86_64-latest.tar"
$EICON_OPENOCD_RELEASE     = "0.11.0"

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering                     echo_args";
    Write-Host "Line $(CurrentLine)  INSTALL_PATH               = ${INSTALL_PATH} ";
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION            = ${ARDUINO_VERSION} ";
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR               = ${BOARD_VENDOR} ";
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR_LC            = ${BOARD_VENDOR_LC} ";
    Write-Host "Line $(CurrentLine)  End Of Arguments             ";
    Write-Host "Line $(CurrentLine)  EICON_OPENOCD_RELEASE      = ${EICON_OPENOCD_RELEASE}";
    Write-Host "Line $(CurrentLine)  MSYS2_LATEST               = $MSYS2_LATEST";	
    Write-Host "Line $(CurrentLine)  MSYS64_PATH                = ${MSYS64_PATH}";            
    Write-Host "Line $(CurrentLine)  DLOAD_PATH                 = ${DLOAD_PATH}";            
    Write-Host "Line $(CurrentLine)  7ZIP_PATH                  = ${7ZIP_PATH}";
    Write-Host "Line $(CurrentLine)  ZIP_PATH                   = ${ZIP_PATH}";
    Write-Host "Line $(CurrentLine)  TAR_PATH                   = ${TAR_PATH}";
	
    Write-Host "Line $(CurrentLine)  USER                       = $env:UserName";            
    Write-Host "Line $(CurrentLine)  Leaving                      echo_args";
}


#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering                     main";    
    Write-Host "Line $(CurrentLine)   Calling                      echo_args";
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"


    Write-Host "Line $(CurrentLine)   Executing                    create_folders";
    create_folders
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                      install_msys64";
    install_msys64
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}

function install_msys64
{
    Write-Host "Line $(CurrentLine)   Entering                     echo_args";
    Write-Host "Line $(CurrentLine)   Installing                   MSYS2..." -ForegroundColor Cyan

    Write-Host "Line $(CurrentLine)   Executing                    cd ${INSTALL_PATH}";
    cd ${INSTALL_PATH}	
    Write-Host "Line $(CurrentLine)   PWD                        = ${PWD}";
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    # download installer
    Write-Host "Line $(CurrentLine)   Executing                    if(Test-path ${ZIP_PATH})";            
    if(Test-path ${ZIP_PATH})
	{
		Write-Host "Line $(CurrentLine)   Msys64 Latest                exists ... continuing";
	}
	else
	{
        Write-Host "Line $(CurrentLine)   Downloading                  MSYS installation package...";		
        Write-Host "Line $(CurrentLine)   Executing                    Invoke-WebRequest -Uri ${MSYS2_LATEST} -Outfile ${ZIP_PATH}";            
        Invoke-WebRequest -Uri ${MSYS2_LATEST} -Outfile ${ZIP_PATH}
    }
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Untaring                     installation package..."
	Write-Host "Line $(CurrentLine)  Executing                    & ${7ZIP_PATH}\7z.exe x ${ZIP_PATH} -y -o"${ZIP_PATH}" | Out-Null";            
    &${7ZIP_PATH}\7z.exe x ${ZIP_PATH} -y -o"${TAR_PATH}" | Out-Null
#Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Unzipping                    installation package..."
    Write-Host "Line $(CurrentLine)  Executing                    & ${7ZIP_PATH}\7z.exe x ${TAR_PATH} -y -o"${INSTALL_PATH}" * -r"
    & ${7ZIP_PATH}\7z.exe x ${TAR_PATH} -y -o"${INSTALL_PATH}" * -r
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # update core packages
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman -Syu --needed --noconfirm '"
    bash 'pacman -Syu --needed --noconfirm --ask=20'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman -Syu --noconfirm'"
    bash 'pacman -Syu --noconfirm'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman -Su --noconfirm'"
    bash 'pacman -Su --noconfirm'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # install packages
    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm VCS'"
    bash 'pacman --sync --noconfirm VCS'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm base-devel'"
    bash 'pacman --sync --noconfirm base-devel'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm msys2-devel'"
    bash 'pacman --sync --noconfirm msys2-devel'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-x86_64-gedit'"
    bash 'pacman --sync --noconfirm mingw-w64-x86_64-gedit'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-x86_64-cmake'"
    bash 'pacman --sync --noconfirm mingw-w64-x86_64-cmake'
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-x86_64-openocd '"
    bash 'pacman --sync --noconfirm mingw-w64-x86_64-openocd '
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-x86_64-arm-none-eabi-gcc '"
    bash 'pacman --sync --noconfirm mingw-w64-x86_64-arm-none-eabi-gcc '
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-x86_64-wxWidgets '"
    bash 'pacman --sync --noconfirm mingw-w64-x86_64-wxWidgets '
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)  Calling                      bash 'pacman --sync --noconfirm mingw-w64-x86_64-codelite '"
    bash 'pacman --sync --noconfirm mingw-w64-x86_64-codelite '
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # cleanup pacman cache
    Write-Host "Line $(CurrentLine)  Executing                    Remove-Item ${MSYS64_PATH}\var\cache\pacman\pkg -Recurse -Force"
    Remove-Item ${MSYS64_PATH}\var\cache\pacman\pkg -Recurse -Force
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    # compact folder
    Write-Host "Line $(CurrentLine)  Compacting                  ${MSYS64_PATH}..."
    compact /c /i /s:${MSYS64_PATH} | Out-Null
#Read-Host -Prompt "Line $(CurrentLine)   Pausing:  Press any key to continue"

    Write-Host "  - OK" -ForegroundColor Green
    Write-Host "MSYS2 installed" -ForegroundColor Green
}

#---------------------------------------------------------
# create_folders
#---------------------------------------------------------
function create_folders
{
    Write-Host "Line $(CurrentLine)  Entering                     create_folders";

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${MSYS64_PATH})";
    if (Test-Path ${MSYS64_PATH})
    {
        Write-Host "Line $(CurrentLine)  Folder                       ${MSYS64_PATH}  Exists ... deleting" ;  
        Remove-Item -Recurse -Force ${MSYS64_PATH}
	}
    else
    {
        Write-Host "Line $(CurrentLine)  Folder                       ${MSYS64_PATH}  does not exist ... continuing" ;  
    }

    Write-Host "Line $(CurrentLine)  Executing                    if (Test-Path ${DLOAD_PATH})";
    if (Test-Path ${DLOAD_PATH})
    {
        Write-Host "Line $(CurrentLine)  Folder                       ${DLOAD_PATH}  Exists" ;  
    }
    else
    {
        Write-Host "Line $(CurrentLine)  Creating                     ${DLOAD_PATH}";
        Write-Host "Line $(CurrentLine)  Executing                    md ${DLOAD_PATH}";
        md ${DLOAD_PATH}
    }
    Write-Host "Line $(CurrentLine)  Leaving                      create_folder";
    Write-Host "Line $(CurrentLine)  ----------------------------------------------------------------------------------------"
}



function bash($command)
{
    Write-Host "Line $(CurrentLine)  Executing                    $command" -NoNewline
    cmd /c start /wait ${MSYS64_PATH}\usr\bin\sh.exe --login -c $command
    Write-Host " - OK" -ForegroundColor Green
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}

#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                      main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

#---------------------------------------------------------
# testing $PATH
# C:\msys64\usr\bin\sh.exe -lc "echo $PATH"
# C:\msys64\usr\bin\bash.exe -lc "echo $PATH"

#C:\msys64\usr\bin\sh.exe -lc "pacman -Sy --noconfirm pacman pacman-mirrors"
#C:\msys64\usr\bin\sh.exe -lc "pacman -Syu --noconfirm"
#C:\msys64\usr\bin\sh.exe -lc "pacman -Syu --noconfirm"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm VCS"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm base-devel"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm msys2-devel"
#C:\msys64\usr\bin\sh.exe -lc "pacman --sync --noconfirm mingw-w64-{x86_64,i686}-toolchain"
#---------------------------------------------------------
