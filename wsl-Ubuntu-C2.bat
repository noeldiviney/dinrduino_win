@echo off
SET MyProcess=VcXsrv.exe
ECHO "Test if %MyProcess% is running"
TASKLIST | FINDSTR /I "%MyProcess%">nul
if %errorlevel% neq 0 (
   ECHO "%MyProcess% is not running"
   start C:\"Program Files"\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl
   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-C2 -u eicon
   EXIT /B
) ELSE (
   ECHO "%MyProcess%" is running
   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-C2 -u eicon
   EXIT /B
)
