#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#
#    Launching the Msys2 in a fully configured way.
#
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        helper_function_1        # Calling helper_function_1
#
#        helper_function_2        # Calling helper_function_2
#    }
#
#    #---------------------------------------------------------
#    # Helper Function 1
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Helper Function 2
#    #---------------------------------------------------------
#    function helper_function_1
#    {
#        do something else
#    }
#
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
#---------------------------------------------------------------------------------
param([string]$DRIVE             = "DRIVE",
      [string]$FOLDER            = "FOLDER",
	  [string]$ARDUINO_VERSION   = "ARDUINO_VERSION",
	  [string]$VENDOR            = "VENDOR",
	  [string]$BOARD             = "BOARD",
      [string]$CPU               = "CPU",
      [string]$PRODUCT           = "PRODUCT",
	  [string]$SKETCH            = "SKETCH",
	  [string]$ARCHITECTURE      = "ARCHITECTURE",
	  [string]$PROTOCOL          = "PROTOCOL",
	  [string]$PROBE             = "PROBE",
	  [string]$WSL_DISTRO        = "WSL_DISTRO" ) ;


#Read-Host -Prompt "Pausing:  Press any key to continue"

$USER              ="$env:UserName"
$COMPUTER_NAME     ="$env:ComputerName"
$WIN_BASE_PATH     = "${DRIVE}:\${FOLDER}"
$WSL_BASE_PATH     = "\\wsl$\${WSL_DISTRO}\${FOLDER}"
$ARDUINO_PATH      = "$WSL_BASE_PATH\arduino-$ARDUINO_VERSION"
$PORTABLE_PATH     = "${ARDUINO_PATH}\portable"
$PREFS_PATH        = "${ARDUINO_PATH}\portable"
$SKETCH_PATH       = "$PORTABLE_PATH\sketchbook\arduino\${VENDOR}"
$BUILD_PATH        = "${SKETCH_PATH}\${PRODUCT}-${SKETCH}"
$SKETCH_NAME       = "${PRODUCT}-${SKETCH}"
$PROFILE_PATH      = "/home/$env:UserName"
$SCRIPT_PATH       = "${DRIVE}:\bin\pwshell"
$OPENOCD_PATH      = "${DRIVE}:\${FOLDER}\openocd\bin"
#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
#$PREFS_PATH = "W:\"
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering               echo_args"
    Write-Host "Line $(CurrentLine)  Arguments              ";
    Write-Host "Line $(CurrentLine)  DRIVE                = ${DRIVE} "
Write-Host "Line $(CurrentLine)  FOLDER                   = ${FOLDER} "
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION      = ${ARDUINO_VERSION} "
    Write-Host "Line $(CurrentLine)  VENDOR               = ${VENDOR} "
    Write-Host "Line $(CurrentLine)  BOARD                = ${BOARD} "
    Write-Host "Line $(CurrentLine)  CPU                  = ${CPU} "
    Write-Host "Line $(CurrentLine)  PRODUCT              = ${PRODUCT} "
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH} "
    Write-Host "Line $(CurrentLine)  ARCHITECTURE         = ${ARCHITECTURE} "
    Write-Host "Line $(CurrentLine)  PROTOCOL             = ${PROTOCOL} "
    Write-Host "Line $(CurrentLine)  PROBE                = ${PROBE} "
    Write-Host "Line $(CurrentLine)  WSL_DISTRO           = ${WSL_DISTRO} "
    Write-Host "Line $(CurrentLine)  PWD                  = ${PWD} "
    Write-Host "Line $(CurrentLine)  USER                 = ${USER} "
    Write-Host "Line $(CurrentLine)  COMPUTER_NAME        = ${COMPUTER_NAME} "
    Write-Host "Line $(CurrentLine)  WIN_BASE_PATH        = ${WIN_BASE_PATH} ";            
    Write-Host "Line $(CurrentLine)  WSL_BASE_PATH        = ${WSL_BASE_PATH} ";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH} ";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = ${PREFS_PATH} ";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = ${SKETCH_PATH} ";            
    Write-Host "Line $(CurrentLine)  BUILD_PATH           = ${BUILD_PATH} ";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH} ";            
    Write-Host "Line $(CurrentLine)  OPENOCD_PATH         = ${OPENOCD_PATH} ";            
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH} ";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}


#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering               main"

    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)   Calling                echo_args"
    echo_args                                                                        # Calling echo_args
#Read-Host -Prompt "Pausing:  Press any key to continue"

    echo "Line $(CurrentLine)   Calling                Launch_jtagit_prog()"
    Launch_jtagit_prog

}


#---------------------------------------------------------
# Edit Profile
#---------------------------------------------------------
function Edit_Profile
{
    Write-Host "Line $(CurrentLine)  Entering               Edit_Profile()"

    Write-Host "Line $(CurrentLine)  Editing                .profile and  replace NEW_VERSION with $VERSION"
    ((Get-Content -path $PROFILE_PATH/.profile -Raw) -replace 'NEW_VERSION', $VERSION) | Set-Content -Path $PROFILE_PATH/.profile

    Write-Host "Line $(CurrentLine)  Leaving                Edit_Profile()"
}

#---------------------------------------------------------
# create_openocd_target
#---------------------------------------------------------
function create_openocd_target
{
    Write-Host "Line $(CurrentLine)  Entering               create_openocd_target()"

    Write-Host "Line $(CurrentLine)  Leaving                create_openocd_target()"
}

#---------------------------------------------------------
# Launch launcf_jtagit_prog
#---------------------------------------------------------
function launch_jtagit_prog
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_jtagit_prog()"
    Write-Host "Line $(CurrentLine)  Protocol             = ${PROTOCOL} "
	Write-Host "Line $(CurrentLine)  Executing              '& ${OPENOCD_PATH}\openocd.exe -f board/eicon/wsl/${PROBE}/${PRODUCT}-${SKETCH}.ino-${PROTOCOL}.cfg' "
    & ${OPENOCD_PATH}\openocd.exe -f board/eicon/wsl/${PROBE}/${PRODUCT}-${SKETCH}.ino-${PROTOCOL}.cfg

#Read-Host -Prompt "Pausing:  Press any key to continue"

#    cd $SCRIPT_PATH
#    Write-Host "Line $(CurrentLine)  PWD                  = $PWD "
    
    Write-Host "Line $(CurrentLine)  Leaving                launch_jtagit_prog"
}

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
echo "Line $(CurrentLine)  Calling                main()"
main
Write-Host "Line $(CurrentLine)  All Done  To exit Press Continue"
Read-Host -Prompt "Line $(CurrentLine)  Pausing:  Press any key to continue"

