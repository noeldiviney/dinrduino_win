#!C:\PgmFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass
#--------------------------------------------------------------
#    Desktop Shortcut Example
#    W:\ProgramFiles\PowerShell\7\pwsh.exe -ExecutionPolicy Bypass -File W:\DinRDuino\PwrShell\arduino_ide.ps1 W DinRDuino 1.8.15 Eicon BluePill F103C8T6 SerLed V1.0.1 stm32 swd
#
#    Launching the Arduino IDE in a fully configured way.
#    This Pwershell Script uses forward declarations as follows.
#
#    #---------------------------------------------------------
#    # Main Function
#    #---------------------------------------------------------
#    function main
#    {
#        function_1        # Calling function_1
#        function_2        # Calling function_2
#    }
#    #---------------------------------------------------------
#    # Function 1
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something
#    }
#    #---------------------------------------------------------
#    # Function 2
#    #---------------------------------------------------------
#    function function_1
#    {
#        do something else
#    }
#    #---------------------------------------------------------
#    # Main      The script entry point
#    #---------------------------------------------------------
#    main                      # Call function_main
#--------------------------------------------------------------

#---------------------------------------------------------
# Global Parameters
#---------------------------------------------------------
param(
      [string]$USERNAME         = "USERNAME",
      [string]$INSTALL_FOLDER   = "INSTALL_FOLDER",
	  [string]$ARDUINO_VERSION  = "ARDUINO_VERSION",
	  [string]$BOARD_VENDOR     = "BOARD_VENDOR",
	  [string]$BOARD            = "BOARD",
      [string]$CPU              = "CPU", 
	  [string]$PRODUCT          = "PRODUCT",
	  [string]$SKETCH           = "SKETCH",
	  [string]$SKETCH_VER       = "SKETCH_VER", 
	  [string]$ARCHITECTURE     = "ARCHITECTURE",
	  [string]$PROTOCOL         = "PROTOCOL",
	  [string]$DISTRO           = "DISTRO",
	  [string]$DISTRO_VHD       = "DISTRO_VHD"
	  ) ;
#Read-Host -Prompt "Pausing:  Press any key to continue"

$WIN_BASE_ROOT     = "C:"
$WSL_BASE_ROOT     = "\\wsl$"
$WSL_HOME          = "/home/eicon"
$WSL_SLASH         = "/"
$WIN_SLASH         = "\"
$XSERVER_NAME      = "vcxsrv"
$XSERVER_STATUS    = ""
$WIN_BASE_PATH     = "${WIN_BASE_ROOT}${WIN_SLASH}${INSTALL_FOLDER}"
$WSL_BASE_PATH     = "${WSL_BASE_ROOT}${WSL_SLASH}${INSTALL_FOLDER}"
$ARDUINO_PATH      = "${WSL_HOME}${WSL_SLASH}${INSTALL_FOLDER}${WSL_SLASH}arduino-${ARDUINO_VERSION}"
$PORTABLE_PATH     = "${ARDUINO_PATH}${WSL_SLASH}portable"
$PREFS_PATH        = "${PORTABLE_PATH}"
$SKETCH_PATH       = "${PORTABLE_PATH}${WSL_SLASH}sketchbook${WSL_SLASH}arduino${WSL_SLASH}${BOARD_VENDOR}${WSL_SLASH}${CPU}"
$SKETCH_NAME       = "${PRODUCT}-${SKETCH}-${SKETCH_VER}"
$SKETCH_PREFS_PATH = "${SKETCH_PATH}${WSL_SLASH}${SKETCH_NAME}${WSL_SLASH}preferences"
$USER              = "$env:USER"                                 # $env:VAR_NAME="VALUE"
$PROFILE_PATH      = "/home/$env:USER"
$SCRIPT_PATH       = "${DRIVE}:\bin\pwshell"
$OPENOCD_PATH      = "${DRIVE}:\bin\openocd\bin"
#Read-Host -Prompt "Pausing:  Press any key to continue"

#---------------------------------------------------------
# Main Function
#---------------------------------------------------------
function main
{
    Write-Host "Line $(CurrentLine)   Entering               main"
#Read-Host -Prompt 'Input your server  name'

    Write-Host "Line $(CurrentLine)   Calling                echo_args"
    echo_args                                                                        # Calling echo_args
Read-Host -Prompt "Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                Launch_VcXsrv"
    Launch_VcXsrv                                                                        # Calling echo_args
Read-Host -Prompt "Pausing:  Press any key to continue"

    Write-Host "Line $(CurrentLine)   Calling                Launch_arduino_ide"
    Launch_arduino_ide                                                                        # Calling echo_args
Read-Host -Prompt "Pausing:  Press any key to continue"

}

#---------------------------------------------------------
# Launch_VcXsrv.exe 
#---------------------------------------------------------
function Launch_arduino_ide
{
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Entering               Launch_arduino_ide"
    Write-Host "Line $(CurrentLine)  Executing              wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c "cp -fr ${SKETCH_PREFS_PATH}${WSL_SLASH}${BOARD}-${SKETCH}-${SKETCH_VER}.txt ${PORTABLE_PATH}${WSL_SLASH}preferences.txt" "
    wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c "cp -fr ${SKETCH_PREFS_PATH}${WSL_SLASH}${BOARD}-${SKETCH}-${SKETCH_VER}.txt ${PORTABLE_PATH}${WSL_SLASH}preferences.txt"
Read-Host -Prompt "Pausing:  Press any key to continue"

#    Write-Host "Line $(CurrentLine)  Executing              wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c  eicon bash "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0  && export LIBGL_ALWAYS_INDIRECT=1 && ~/DinRDuino/arduino-%ARDUINO_VER%/arduino"
#    wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c  eicon bash "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0  && export LIBGL_ALWAYS_INDIRECT=1 && ~/DinRDuino/arduino-%ARDUINO_VER%/arduino"

#    Write-Host "Line $(CurrentLine)  Executing              wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c "export DISPLAY=$(cat /etc/resolv.conf | ggrep nameserver | awk '{print $2; exit;}'):0.0 " "
    wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0 "
Read-Host -Prompt "Pausing:  Press any key to continue"


Write-Host "Line $(CurrentLine)  Executing              wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c "~${WSL_SLASH}DinRDuino${WSL_SLASH}arduino-${ARDUINO_VERSION}${WSL_SLASH}arduino""
Read-Host -Prompt "Pausing:  Press any key to continue"
    wsl -d ${DISTRO}-${DISTRO_VHD} -u ${USERNAME} bash -c "~${WSL_SLASH}DinRDuino${WSL_SLASH}arduino-${ARDUINO_VERSION}${WSL_SLASH}arduino"
Read-Host -Prompt "Pausing:  Press any key to continue"
    Write-Host "Line $(CurrentLine)  Leaving               Launch_arduino_ide"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}
#---------------------------------------------------------
# Launch_VcXsrv.exe 
#---------------------------------------------------------
function Launch_VcXsrv
{
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
    Write-Host "Line $(CurrentLine)  Entering               Launch_VcXsrv"
    Write-Host "Line $(CurrentLine)  Executing              if((Get-Process "${XSERVER_NAME}" -ea SilentlyContinue) -eq $Null)"
    if((Get-Process "${XSERVER_NAME}" -ea SilentlyContinue) -eq $Null)
	{ 
	    Write-Host "Line $(CurrentLine)  ${XSERVER_NAME}                 is not running ... launching ${XSERVER_NAME}"
	Write-Host "Line $(CurrentLine)  Executing              Start-Process -NoNewWindow -FilePath ${WIN_BASE_ROOT}${WIN_SLASH}"Program Files"${WIN_SLASH}${XSERVER_NAME}${WIN_SLASH}${XSERVER_NAME}.exe -ArgumentList "-ac", "-terminate", "-lesspointer", "-multiwindow", "-clipboard", "-wgl""
		Start-Process -NoNewWindow -FilePath ${WIN_BASE_ROOT}${WIN_SLASH}"Program Files"${WIN_SLASH}${XSERVER_NAME}${WIN_SLASH}${XSERVER_NAME}.exe -ArgumentList "-ac", "-terminate", "-lesspointer", "-multiwindow", "-clipboard", "-wgl"
    }
    else
	{ 
        Write-Host "Line $(CurrentLine)  ${XSERVER_NAME}                 is running ... continuing"    
    }
    Write-Host "Line $(CurrentLine)  Leaving                Launch_VcXsrv"
    Write-Host "Line $(CurrentLine)   ----------------------------------------------------------------------------------------"
}
#Read-Host -Prompt "Pausing:  Press any key to continue"


#---------------------------------------------------------
# Echo Args
#---------------------------------------------------------
function echo_args
{
$MyVariable = 1

    Write-Host "Line $(CurrentLine)  Entering               echo_args"
    Write-Host "Line $(CurrentLine)  Parameters             begin"
    Write-Host "Line $(CurrentLine)  USERNAME             = ${USERNAME}"
    Write-Host "Line $(CurrentLine)  INSTALL_FOLDER       = ${INSTALL_FOLDER}"
    Write-Host "Line $(CurrentLine)  ARDUINO_VERSION      = ${ARDUINO_VERSION}"
    Write-Host "Line $(CurrentLine)  BOARD_VENDOR         = ${BOARD_VENDOR}"
    Write-Host "Line $(CurrentLine)  BOARD                = ${BOARD}"
    Write-Host "Line $(CurrentLine)  CPU                  = ${CPU}" 
    Write-Host "Line $(CurrentLine)  PRODUCT              = ${PRODUCT}"
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH}"
    Write-Host "Line $(CurrentLine)  SKETCH_VER           = ${SKETCH_VER}" 
    Write-Host "Line $(CurrentLine)  ARCHITECTURE         = ${ARCHITECTURE}"
    Write-Host "Line $(CurrentLine)  PROTOCOL             = ${PROTOCOL}"
    Write-Host "Line $(CurrentLine)  DISTRO               = ${DISTRO}"
    Write-Host "Line $(CurrentLine)  DISTRO_VHD           = ${DISTRO_VHD}"
    Write-Host "Line $(CurrentLine)  Parameters             end"
    Write-Host "Line $(CurrentLine)  WIN_BASE_ROOT        = ${WIN_BASE_ROOT}"
    Write-Host "Line $(CurrentLine)  WSL_BASE_ROOT        = ${WSL_BASE_ROOT}"
    Write-Host "Line $(CurrentLine)  WIN_SLASH            = ${WIN_SLASH}"
    Write-Host "Line $(CurrentLine)  WSL_SLASH            = ${WSL_SLASH}"
	Write-Host "Line $(CurrentLine)  XSERVER_NAME         = ${XSERVER_NAME} "
    Write-Host "Line $(CurrentLine)  WIN_BASE_PATH        = ${WIN_BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  WSL_BASE_PATH        = ${WSL_BASE_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  PREFS_PATH           = ${PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_NAME          = ${SKETCH_NAME}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PATH          = ${SKETCH_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH_PREFS_PATH    = ${SKETCH_PREFS_PATH}";            
    Write-Host "Line $(CurrentLine)  ARDUINO_PATH         = ${ARDUINO_PATH}";            
    Write-Host "Line $(CurrentLine)  SKETCH               = ${SKETCH}";            
    Write-Host "Line $(CurrentLine)  Leaving                echo_args"
}

<#
#---------------------------------------------------------
# Launch_Arduino_IDE
#---------------------------------------------------------
function Launch_Arduino_IDE
{
    Write-Host "Line $(CurrentLine)  Entering               Launch_Arduino_IDE"

    Write-Host "Line $(CurrentLine)  Executing              $ARDUINO_PATH\arduino "
#Read-Host -Prompt "Pausing:  Press any key to continue"
    & $ARDUINO_PATH\arduino
    Write-Host "Line $(CurrentLine)  Leaving                Launch_Arduino_IDE"
}
#>

#---------------------------------------------------------
# CurrentLine
#---------------------------------------------------------
function CurrentLine
{
    $MyInvocation.ScriptLineNumber
}


#---------------------------------------------------------
# Main Entry point
#---------------------------------------------------------
Write-Host "Line $(CurrentLine)  Calling                main()"
main
