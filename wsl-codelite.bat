@echo off
REM Test if VcXsrv,exe is running and if not Launch it
REM Launch Ubuntu-18.04-W with selected user-name
echo %1%
SET MyProcess=VcXsrv.exe
SET ARDUINO_VERSION=%1
SET VENDOR=%2
SET BOARD=%3
SET CPU=%4
SET PRODUCT=%5
SET SKETCH=%6
SET DISTRO=%7
SET SKETCH_PREFS_PATH=/home/eicon/DinRDuino/arduino-%ARDUINO_VERSION%/portable/sketchbook/arduino/%VENDOR%/%PRODUCT%-%SKETCH%
SET PORTABLE_PATH=/home/eicon/DinRDuino/arduino-%ARDUINO_VERSION%/portable
echo "MyProcess            = %MyProcess%
echo "ARDUINO VERSION      = %ARDUINO_VERSION% "
echo "VENDOR               = %VENDOR% "
echo "BOARD                = %BOARD% "
echo "CPU                  = %CPU% "
echo "PRODUCT              = %PRODUCT% "
echo "SKETCH               = %SKETCH% "
echo "DISTRO               = %DISTRO%
echo "SKETCH_PREFS_PATH    = %SKETCH_PREFS_PATH% "
echo "PORTABLE_PATH        = %PORTABLE_PATH% "
pause
ECHO "Test if %MyProcess% is running"
TASKLIST | FINDSTR /I "%MyProcess%">nul
if %errorlevel% neq 0 (
   ECHO "%MyProcess% is not running"
REM pause
   start C:\"Program Files"\VcXsrv\vcxsrv.exe -ac -terminate -lesspointer -multiwindow -clipboard -wgl
REM   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-%DISTRO% -u eicon bash -c "cp -fr %SKETCH_PREFS_PATH%/preferences/%PRODUCT%-%SKETCH%.txt %PORTABLE_PATH%/preferences.txt"
   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-%DISTRO% -u eicon bash -c "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0  && export LIBGL_ALWAYS_INDIRECT=1 && codelite"
   REM   start %windir%\system32\mstsc.exe
   EXIT /B
) ELSE (
   ECHO "%MyProcess% is running"
REM   ECHO "Executing ... start %windir%\System32\cmd.exe /K wsl.exe -d %DISTRO% -u eicon bash -c "cp -fr %SKETCH_PREFS_PATH%/preferences/%PRODUCT%-%SKETCH%.txt %PORTABLE_PATH%/preferences.txt""
pause
REM   start %windir%\System32\cmd.exe /K wsl.exe -d %DISTRO% -u eicon bash -c "cp -fr %SKETCH_PREFS_PATH%/preferences/%PRODUCT%-%SKETCH%.txt %PORTABLE_PATH%/preferences.txt"
REM pause
REM   ECHO "Executing ... start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-%VHD% -u eicon bash -c "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0  && export LIBGL_ALWAYS_INDIRECT=1 && ~/DinRDuino/arduino-%ARDUINO_VERSION%/arduino""

   start %windir%\System32\cmd.exe /C wsl.exe -d %DISTRO% -u eicon bash -c "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0  && export LIBGL_ALWAYS_INDIRECT=1 && codelite"


REM pause
REM   start %windir%\System32\cmd.exe /K wsl.exe -d Ubuntu-%VHD% -u eicon bash -c "export DISPLAY=$(cat /etc/resolv.conf | grep nameserver | awk '{print $2; exit;}'):0.0  && export LIBGL_ALWAYS_INDIRECT=1 && ~/DinRDuino/arduino-%ARDUINO_VER%/arduino"
REM   start %windir%\system32\mstsc.exe
pause
   EXIT /B
)	
